{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "# Welcome to zesje\n",
    "\n",
    "*Zesje* is an online software for grading exams.\n",
    "\n",
    "As its name suggests, Zesje has been designed as a **minimal working prototype** during our free time so do not expect any degree of polish. Still it can provide a lot of advantages over manual grading, especially when dealing with large courses.\n",
    "\n",
    "This notebook is a tutorial and a reference documentation for Zesje. If you want to try out different software features, there is a sandbox deployment available at https://sandbox.grading.quantumtinkerer.tudelft.nl; try it out without fear of breaking things. Because of spam concerns, its email sending capabilities are limited to the [`sharklasers.com`](https://sharklasers.com) domain that provides temporary mailboxes accessible by anyone."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "## Benefits of using zesje\n",
    "\n",
    "+ Moving the grading online: you don't need to deal with paper, updating and processing grades is automated. The grading can take place from anywhere.\n",
    "+ Zesje is faster than paper-based grading, especially for large courses\n",
    "+ Make grading streamlined by allowing to follow a grading scheme that is both clearly defined and easy to adjust.\n",
    "+ Export of grades in various formats for postprocessing.\n",
    "+ Quick built-in analysis for a progress overview.\n",
    "+ Ability to email students their solutions together with detailed feedback. From the student perspective this is the killer feature.\n",
    "+ Fully open source: if you want to modify anything and implemennt new functionality, you are welcome to do so."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "## Limitations and disclaimer\n",
    "\n",
    "+ **Zesje is a prototype-level software**: it is tested by people using it. It is not developed by professional web developers, and it certainly has many rough edges.\n",
    "+ If something breaks, we are very sorry. Most likely we'd be able to fix your problem, but we do not provide any guarantees.\n",
    "+ Fully trusted users: anyone with the login and password for your zesje installation can do anything.\n",
    "+ No change history. We do daily backups, but you cannot revert your changes manually."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "## Support\n",
    "\n",
    "If you want to use zesje in a course, we are happy to host it for you, contact us at [zesje@antonakhmerov.org](mailto:zesje@antonakhmerov.org) or [me@josephweston.org](mailto:me@josephweston.org).\n",
    "\n",
    "If you have questions about using zesje, please use one of the two options: if you have a question, use the zesje support chat at https://chat.quantumtinkerer.tudelft.nl/external/channels/zesje (you'll need to create an account and join the **external** team after creating it).\n",
    "\n",
    "If you found a bug or if you have a suggestion for improvement, please report it at Zesje [issue tracker](https://gitlab.kwant-project.org/zesje/zesje/issues)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "## Initializing a course\n",
    "\n",
    "Each course will need its own zesje installation, but a single installation can be used to grade multiple exams.\n",
    "\n",
    "### Adding students\n",
    "1. Export the student information from Brightspace. To do that go to the course page, select *Grades* → *Enter grades* → *Export grades*. Then choose the options as shown: ![](../files/brightspace_export.png)\n",
    "2. After exporting the csv do the following edits:\n",
    "   - Remove the first line with field names\n",
    "   - Remove all occurrences of `,#` at the end of each line\n",
    "   - Remove all occurrences of `#` at the beginning of each line\n",
    "   - Remove/edit all entries that do not have the correct student id format.\n",
    "3. Browse to the [add-graders-and-students](../add-graders-and-students.ipynb) dashboard and select the *students* tab.\n",
    "4. Copy/paste the contents of the csv file into the text area and click *add students*. You should see a green check mark appear next to the button and the student information should appear to the right of the text area. ![](../files/add-students.png)\n",
    "\n",
    "You can add more students and edit existing student information at any time. It is currently not possible to remove students from zesje.\n",
    "\n",
    "You can also use any other source of the student information, and you can add students manually.\n",
    "\n",
    "### Adding graders\n",
    "1. Browse to the [add-graders-and-students](../add-graders-and-students.ipynb) dashboard and select the *graders* tab.\n",
    "2. Add grader information to the text area, one grader per line. and click *add graders\". ![](../files/add-graders.png)\n",
    "\n",
    "It is currently not possible to modify or remove graders from zesje.\n",
    "\n",
    "The different graders are only used to identify who graded a particular solution.\n",
    "\n",
    "There is only a single set of login credentials for a zesje instance, so anyone can in principle \"impersonate\" anyone else."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "## Creating an exam\n",
    "\n",
    "### Make a source latex document\n",
    "\n",
    "While you can use any software to create a printed exam, we are using automatically generated QR codes, and we have written a script that does so using LaTex. Therefore the main option for creating exams right now is to use LaTeX.\n",
    "\n",
    "The step-by-step instructions are as follows:\n",
    "1. Get the necessary [package file](../files/exam.sty) and the [mini-form](../files/number_widget.pdf) for providing the student number\n",
    "2. Depending on your latex installation, you might also need the [qrcode](https://www.ctan.org/tex-archive/macros/latex/contrib/qrcode) LaTeX package provided for most popular LaTeX distributions.\n",
    "3. For details on making the exam, check out a detailed example of an exam used for zesje sandbox [over here](https://gitlab.kwant-project.org/zesje/demo-data/blob/master/raw_data/test-1/manuscript/test-1.tex).  \n",
    "   The following is the minimal skeleton of an exam source file:  \n",
    "\n",
    "```latex\n",
    "\\documentclass[11pt, twoside]{article}\n",
    "\\usepackage[margin=1.5cm]{geometry}\n",
    "\\usepackage[name=test-1, copies=100]{exam}\n",
    "\\begin{document}\n",
    "\\begin{exam}\n",
    "\\answerbox{4cm}{}\n",
    "\\end{exam}\n",
    "\\end{document}\n",
    "```\n",
    "\n",
    "#### Important warnings:\n",
    "+ Do not use a title page, they are currently not supported.\n",
    "+ The exam name provided in the header **must be unique**. Otherwise you won't be able to import it in zesje.\n",
    "+ When developing and trying an exam form you will want to use `copies=1` for speed. Use a reasonable number of copies when printing the final version. \n",
    "+ Since every copy of an exam should have a unique copy number, you shouldn't print the same exam twice.\n",
    "+ Use sufficient space for each answer, and provide an extra answerbox at the end of the exam in case the students run out of space. If that fails, have a student submit **two complete copies of the exam**.\n",
    "\n",
    "### Create the exam in zesje\n",
    "Once the exams have been printed you can tell zesje about the new exam by uploading the metadata file generated during the latex compilation.\n",
    "\n",
    "1. Browse to the [upload](../upload.ipynb) dashboard and select the *Upload exam metadata* tab\n",
    "2. Select *None* in the *Exam* dropdown list at the top of the dashboard\n",
    "3. Copy/paste the contents of the **.yml** file (generated when compiling the latex document) into the text area, and click *Upload metadata*.\n",
    "   If the upload was successful a green checkmark should appear. ![](../files/upload-metadata.png)\n",
    "   \n",
    "It is possible to tweak the metadata once it is uploaded. Simply select the *Upload exam metadata* tab and the correct exam in the *Exam* dropdown to see the currently uploaded metadata. After making your changes, be sure to click *Upload metadata* to save your changes.\n",
    "\n",
    "The main uses of tweaking the metadata are to:\n",
    "\n",
    "+ **rename problems**. By default zesje numbers the exam problems automatically, and it is unlikely that zesje's names correspond to the problem names used in the exam manuscript (e.g. problems *1a* and *1b* would be denoted *question_1* and *question_2* in zesje by default). You may change the *name* fields in the metadata to rename your questions accordingly. *Do not rename the \"qrcode\" metadata elements*; these are used to identify different pages in the scanned exams.\n",
    "+ **adjust the viewport when grading**. If the exam was scanned at a jaunty angle, or with an offset, then the metadata that specifies the area on the page where the answer boxes are will be incorrect. The *bottom*, *top*, *left* and *right* metadata can be modified to change the positions of the sides of the viewport."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "## Scanning and uploading the solutions\n",
    "\n",
    "### Find a good scanner\n",
    "\n",
    "There are two types of scanners in TUD: Ricoh Alficio 3001 and Ricoh Alficio 4501. The 4501 model will take several minutes for a large exam, while 3001 lasts more than an hour. Find the 4501 model and use it (there is one in F183 for example).\n",
    "\n",
    "### Scanner settings\n",
    "\n",
    "#### Select the \"My Home Folder\" option:\n",
    "\n",
    "![](../files/scan_to_me.jpg)\n",
    "\n",
    "#### Select the following scanner settings:\n",
    "\n",
    "![](../files/scan_settings.jpg)\n",
    "\n",
    "#### Use the paper feed, as shown:\n",
    "\n",
    "![](../files/paper_feed.jpg)\n",
    "\n",
    "### Uploading the scan results\n",
    "\n",
    "**Note:** it may take up to an hour after the scanning has physically stopped for the scanners to generate a pdf for large exams.\n",
    "\n",
    "Once it's done, get it from your webdata by any means, for example at `https://webdata.tudelft.nl/staff-homes/<first letter of last name>/<netid>/My Documents/`\n",
    "\n",
    "The next step is to make the pdf file available for download via URL. Here we show how to do it with dropbox, but [surfdrive](https://surfdrive.nl) is another viable option.\n",
    "\n",
    "#### Step 1\n",
    "\n",
    "![](../files/share_file.png)\n",
    "\n",
    "#### Step 2\n",
    "\n",
    "![](../files/share_link.png)\n",
    "\n",
    "#### Step 3\n",
    "\n",
    "Finally, select the correct exam in the [upload](../upload.ipynb) dashboard, and paste the resulting link:\n",
    "\n",
    "![](../files/upload_exam.png)\n",
    "\n",
    "**Note:** If you reupload scans of the same pages, these will overwrite the older uploads. Therefore you can always fix problems later if your initial upload isn't good enough.\n",
    "\n",
    "### Verifying the students\n",
    "\n",
    "Check that the students are identified correctly in the [check_student_numbers](../check_student_numbers.ipynb) dashboard. This is necessary to ensure that all the student numbers are identified correctly.\n",
    "\n",
    "This is done as shown in the [**video**](../files/validate.ogv)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "## Grading an exam\n",
    "\n",
    "### Video summary:\n",
    "\n",
    "Watch the [**video**](../files/grade.ogv) showing the typical workflow.\n",
    "\n",
    "### Assiging grades and feedback\n",
    "+ Select the feedback options\n",
    "+ Add any \"specific\" feedback for a particular solution, but this is for information purposes only, and does not add/subtract points\n",
    "\n",
    "### Editing the grading scheme\n",
    "+ Based on the idea that you can student answers tend to have similarities\n",
    "+ Zesje allows you to adapt your feedback to student responses while grading\n",
    "+ You don't need to decide on a final grading scheme beforehand\n",
    "\n",
    "### Choosing a grading scheme\n",
    "\n",
    "While you are completely free in choosing the grading scheme, and you should choose the workflow that works best for you, there are several things that you may want to follow.\n",
    "\n",
    "+ **Important**: Zesje uses integers for all scores. Ensure that the smallest amount of score differences is 1 point.\n",
    "+ Keep one option with 0 points, and one with maximal score for the problem.\n",
    "+ Since the feedback the students see is based on the feedback options you select, for most cases it makes sense to give the students a maximal score and subtract some points for the omissions they made.\n",
    "+ Choose short and easy to remember names for the feedback options. In the extended description try to outline the part of the solution. The students can see this in an email.\n",
    "\n",
    "### Modifying a specific student\n",
    "\n",
    "If you need to navigate to a specific student, use the \"jump to student\" text field. Thanks to autocompletion, it allows you to search a student by name or their student number."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "## Processing results\n",
    "\n",
    "Most of the parts should be self-explanatory, however let us list the currently available options:\n",
    "\n",
    "+ **Problem statistics and scores**:  \n",
    "  Available in the *problem statistics* tab of the [grade](../grade.ipynb) dashboard, it shows a quick-and-dirty summary of the problem grading.\n",
    "+ **Exam statistics report**:  \n",
    "  Available in the *summary & email* tab of the [grade](../grade.ipynb) dashboard. A rendered html summary with the detailed but anonymized data that shows the score distributions, all the feedback options, scores, and clarifications. Depending on your preferences, it may be shared with the students.\n",
    "+ **Export grades**  \n",
    "  Available in the *summary & email* tab of the [grade](../grade.ipynb) dashboard.\n",
    "  - **Spreadsheet (detailed)**: an Excel file with full information about the exam.\n",
    "  - **Spreadsheet (summary)**: an Excel file containing only scores per problem.\n",
    "  - **DafaFrame (detailed)**: a [pandas](https://pandas.pydata.org) dataframe with all information, useful for advanced data analysis.\n",
    "  - **Complete database**: the full internal database, can be used to back up the data and restore (albeit the restoring will require our help)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "## Sending feedback\n",
    "\n",
    "### Prepare an email template\n",
    "In the *Individual email* tab of the [grade](../grade.ipynb) dashboard you can create an template that will be used to send personalized feedback to the students.\n",
    "\n",
    "**Warning**: templates are currently not saved, copy them externally if you want to edit them afterwards!\n",
    "\n",
    "The template is written using the [Jinja 2](http://jinja.pocoo.org/docs/2.9/templates/#template-designer-documentation) templating language. This allows you to intermix regular text with special commands that will do things like optionally include some text if a certain condition is satisfied, or print the contents of some variable (such as the student's name or grade).\n",
    "\n",
    "Although you can find a comprehensive language reference on the Jinja website, we include some basic usage below.\n",
    "\n",
    "#### Printing variables\n",
    "Type a variable name inside double curly braces (`{{`) to have the variable printed in the document, e.g.\n",
    "```\n",
    "{{ first_name }}\n",
    "```\n",
    "expands to the first name of the student that the template is rendering. Variables can also have *attributes*, which can\n",
    "be accessed with `.` inside a variable expansion, e.g.\n",
    "```\n",
    "your grade is: {{ student.total }}\n",
    "```\n",
    "Jinja also includes so-called \"filters\" that can be used to modify the way a variable is printed. For example,\n",
    "```\n",
    "{{ student.first_name | uppercase }}\n",
    "```\n",
    "will print the student's first name in uppercase. The Jinja website hasa complete list of available filters.\n",
    "\n",
    "#### Optionally including text\n",
    "Include optional text between `{% if <condition> %}` and `{% endif %}` tags, e.g.\n",
    "```\n",
    "This text is always included\n",
    "{% if student.total > 80 %}\n",
    "Well done {{student.first_name}}; you did very well.\n",
    "{% endif %}\n",
    "```\n",
    "\n",
    "#### Looping\n",
    "It is possible to loop with the `{% for <item> in <list> %}` `{% endfor %}` tags, e.g.\n",
    "```\n",
    "{% for feedback in problem.feedback %}\n",
    "  your feedback: {{ feedback.short }}\n",
    "{% endfor %}\n",
    "```\n",
    "\n",
    "#### List of variables\n",
    "Within your templates you can use the **`student`** and **`results`** variables. `student` has several attributes, and `results` is a list (that you can use in a `{% for %}` loop).\n",
    "\n",
    "##### `student`\n",
    "+ `first_name`: Student's first name\n",
    "+ `last_name`: Student's last name\n",
    "+ `email`: Student's email address\n",
    "+ `total`: Sum of the `score`s for the student's problems\n",
    "\n",
    "##### elements of `results`\n",
    "+ `name`: Problem name\n",
    "+ `max_score`: Maximum score for the problem\n",
    "+ `feedback`: List of feedback the student got for this problem. Each   \n",
    "              element has the following attributes:\n",
    "  - `short`: Short description of the feedback, as used when grading\n",
    "  - `score`: Score associated with this feedback\n",
    "  - `description`: Long description of the feedback\n",
    "+ `score`: Student's score for this problem\n",
    "+ `remarks`: Student-specific feedback"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "grid_default": {},
       "report_default": {
        "hidden": false
       }
      }
     }
    }
   },
   "source": [
    "### Send feedback to indivdual students or everyone\n",
    "\n",
    "After composing the template you can press the green *Refresh* button to render the template for the current student. It is a good idea to do this for a few students before sending any feedback by email, to make sure that your template is working.\n",
    "\n",
    "From the same tab (*Individual email*) you can also email the rendered template *for the currently selected student*. It is a good idea to do this for several students and email the results to yourself (by supplying your email in the *Recipients* text box) to ensure that the output is satisfactory. By default the email is not sent to the student.\n",
    "\n",
    "To send personalized emails to all students, use the *Email results to everyone* button in the *summary & email* tab in the [grade](../grade.ipynb) dashboard. You must first enable this button by checking the *Yes, email everyone* checkbox. Note that sending the emails to everyone may take some time, and it is impossible to interrupt this process. That being said, do not browse away from the *summary & email* tab until the process is completed; the blue progress bar on the top of the screen will disappear.\n",
    "You will see an error message if some emails were not sent."
   ]
  }
 ],
 "metadata": {
  "extensions": {
   "jupyter_dashboards": {
    "activeView": "report_default",
    "version": 1,
    "views": {
     "grid_default": {
      "name": "grid",
      "type": "grid"
     },
     "report_default": {
      "name": "report",
      "type": "report"
     }
    }
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
